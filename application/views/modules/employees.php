
    <!-- Header -->
    <!-- Header -->
    <br><br><br><br>
    <!-- Page content -->
    <div class="container-fluid mt--6">
    
    <center><h1>Employees</h1></center>
      <div class="row">
            <small>
                <a href="javascript:void(0)" onclick="create()">
                    <i class="fa fa-plus"></i> Create Employee
                </a>
            </small>
        <hr>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>NAME</th>
                        <th>COMPANY</th>
                        <th>DEPARTMENT</th>
                        <th>STATUS</th>
                        <th>DESCRIPTION</th>
                        <th>MODIFIED BY</th>
                        <th>MODIFIED AT</th>
                        <th>&nbsp;</th>
                    </tr>
                    <tbody id="emp_here"></tbody>
                </thead>

            </table>
      </div>
      <!-- Footer -->
    
  <script src="<?php echo base_url();?>assets/app/js/employee.js"></script>