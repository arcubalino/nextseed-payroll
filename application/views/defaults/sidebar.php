
<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          NextSeed
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" href="<?php echo base_url();?>">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('companies');?>">
                <i class="ni ni-planet text-orange"></i>
                <span class="nav-link-text">Companies</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('departments');?>">
                <i class="ni ni-pin-3 text-primary"></i>
                <span class="nav-link-text">Departments</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('employees');?>">
                <i class="ni ni-single-02 text-yellow"></i>
                <span class="nav-link-text">Employees</span>
              </a>
            </li>
           
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->