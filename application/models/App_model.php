<?php

class App_model extends CI_Model {

	function __construct(){
		$this->load->library('globalcall');
		$this->db_next = $this->load->database('db_next',TRUE);
	}

    function process_validation(){
        $email_add = $this->input->post('email_add');
		$password = md5($this->input->post('password'));
		// print_r($email); exit;
		$this->db_next->where('email',$email_add);
		$this->db_next->where('password',$password);
		$this->db_next->where('status',0);
		$query = $this->db_next->get('users');
		$count = $query->num_rows();

		if($count > 0){
			$rows = $query->row_array();
			$this->globalcall->session_make($rows['email'],$rows['id']);
			return TRUE;
		}else{
			return FALSE;
		}
    }

    function process_register(){
        $email = $this->input->post('email');

        if($this->check_exist_account($email) > 0){
            echo "exist";
            exit;
        }else{
            $pass = $this->input->post('pass');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $sys_now = $this->system_time('now');
            $arr = array(
                'email' => $email,
                'password' => md5($pass),
                'first_name' => $first_name,
                'last_name' => $last_name,
                'created_at' => $sys_now
            );
    
            $query = $this->db_next->insert('users',$arr);
    
            return $query;
        }
      
    }

    function check_exist_account($email){
        $this->db_next->where('email',$email);
        $this->db_next->where('status',0);
        $query = $this->db_next->get('users');

        return $query->num_rows();
    }


    function system_time($action){
		switch($action){
			case "now":
				//return date("Y-m-d G:i:s");
				$qry = "SELECT NOW() AS now_e FROM users LIMIT 1";
			break;
			case "date":
				$qry = "SELECT CURDATE() AS now_e FROM users LIMIT 1";
			break;
			case "time":
				$qry = "SELECT CURTIME() AS now_e FROM users LIMIT 1";
			break;
		}

		$query = $this->db_next->query($qry);
        if($query->num_rows() == 0){
            return date("Y-m-d G:i:s");
        }else{

            $row = $query->row_array();
            return $row['now_e'];
        }
	}

}//end of class