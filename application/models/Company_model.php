<?php

class Company_model extends CI_Model {

	function __construct(){
		$this->load->library('globalcall');
        $this->load->model('App_model','AM');
		$this->db_next = $this->load->database('db_next',TRUE);
	}

    function list_comp(){
        
		$next_id = $this->globalcall->return_decode($this->session->userdata('next_id'));
        $arr = array();

        $query = $this->db_next->query("SELECT comp.id as comp_id,comp.name,comp.address,comp.description,comp.modified_at,
        CONCAT_WS(', ',us.last_name,us.first_name) as `user_name`
        FROM companies as comp
        LEFT JOIN users as us ON us.id=comp.modified_by
        WHERE comp.modified_by =? AND comp.status=?
        ORDER BY comp.name",array($next_id,0));

        foreach($query->result_array() as $row){
            $insert_arr = array(
                'comp_id' => $this->globalcall->post_encode($row['comp_id']),
                'name' => $row['name'],
                'address' => $row['address'],
                'description' => $row['description'],
                'user_name' => $row['user_name'],
                'modified_at' => $row['modified_at'],
            );

            array_push($arr,$insert_arr);
        }

        echo json_encode($arr);
    }


    function process_company(){
        
		$next_id = $this->globalcall->return_decode($this->session->userdata('next_id'));
        $sys_time = $this->AM->system_time('now');
        $comp_name = $this->input->post('comp_name');
        $comp_add = $this->input->post('comp_add');
        $comp_desc = $this->input->post('comp_desc');
        $comp_id = $this->globalcall->post_decode($this->input->post('comp_id'));
        $action = $this->input->post('action');
       
        switch($action){

            case "save":
                $arr = array(
                    'name' => $comp_name,
                    'address' => $comp_add,
                    'description' => $comp_desc,
                    'modified_by' => $next_id,
                    'modified_at' => $sys_time,
                );

                if(empty($comp_id)){//create
                    $query = $this->db_next->insert('companies',$arr);
                }elseif(!empty($comp_id)){//update
                    $this->db_next->where('id',$comp_id);
                    $query = $this->db_next->update('companies',$arr);
                }
            break;

            case "remove":
                $this->db_next->where('id',$comp_id);
                $query = $this->db_next->update('companies',array('status'=>1,'modified_by'=>$next_id,'modified_at'=>$sys_time));

            break;
        }

        return $query;

    }

}//end of class