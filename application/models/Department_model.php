<?php

class Department_model extends CI_Model {

	function __construct(){
		$this->load->library('globalcall');
        $this->load->model('App_model','AM');
		$this->db_next = $this->load->database('db_next',TRUE);
	}


    function list_dept($comp_id){

        $next_id = $this->globalcall->return_decode($this->session->userdata('next_id'));
        $arr = array();
        
        if($comp_id!=0){
            
            $company_id = $this->globalcall->post_decode($comp_id);
            $query = $this->db_next->query("SELECT dept.id as dept_id,dept.name as dept_name,dept.modified_at,dept.company_id,
            CONCAT_WS(', ',us.last_name,us.first_name) as `user_name`,
            comp.name as company_name
            FROM departments as dept
            LEFT JOIN companies as comp ON comp.id=dept.company_id
            LEFT JOIN users as us ON us.id=comp.modified_by 
            WHERE dept.status =? AND comp.status=? AND comp.company_id=?
            ORDER BY dept.name",array(0,0,$company_id));
        }else{

            $query = $this->db_next->query("SELECT dept.id as dept_id,dept.name as dept_name,dept.modified_at,dept.company_id,
            CONCAT_WS(', ',us.last_name,us.first_name) as `user_name`,
            comp.name as company_name
            FROM departments as dept
            LEFT JOIN companies as comp ON comp.id=dept.company_id
            LEFT JOIN users as us ON us.id=comp.modified_by
            WHERE dept.status =? AND comp.status=?
            ORDER BY dept.name",array(0,0));
        }

        foreach($query->result_array() as $row){
            $insert_arr = array(
                'dept_id' => $this->globalcall->post_encode($row['dept_id']),
                'company_id' => $this->globalcall->post_encode($row['company_id']),
                'dept_name' => $row['dept_name'],
                'modified_at' => $row['modified_at'],
                'user_name' => $row['user_name'],
                'company_name' => $row['company_name'],
            );

            array_push($arr,$insert_arr);
        }

        echo json_encode($arr);
    }


    function process_dept(){
        $next_id = $this->globalcall->return_decode($this->session->userdata('next_id'));
        $sys_time = $this->AM->system_time('now');
        $dept_name = $this->input->post('dept_name');
        $company_id = $this->globalcall->post_decode($this->input->post('company_id'));
        $dept_id = $this->globalcall->post_decode($this->input->post('dept_id'));
        $action = $this->input->post('action');
       
        switch($action){

            case "save":
                $arr = array(
                    'name' => $dept_name,
                    'company_id' => $company_id,
                    'modified_by' => $next_id,
                    'modified_at' => $sys_time,
                );

                if(empty($dept_id)){//create
                    $query = $this->db_next->insert('departments',$arr);
                }elseif(!empty($dept_id)){//update
                    $this->db_next->where('id',$dept_id);
                    $query = $this->db_next->update('departments',$arr);
                }
            break;

            case "remove":
                $this->db_next->where('id',$dept_id);
                $query = $this->db_next->update('departments',array('status'=>1,'modified_by'=>$next_id,'modified_at'=>$sys_time));

            break;
        }

        return $query;
    }

}//end of class
?>