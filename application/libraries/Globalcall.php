<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use Hashids\Hashids;
use MatthiasMullie\Minify;

class Globalcall{

	function __construct(){

		$this->CI =& get_instance();
		$this->CI->load->helper('url');
		$this->CI->load->library('session');
		$this->CI->config->item('base_url');
		//$this->CI->load->library('encrypt');
		$this->CI->load->model('App_model',"AM");
        $this->CI->hashids = new Hashids('',12);

    }


    function post_encode($data){
		return $this->CI->hashids->encode($data);
	}

	function post_decode($data){
		$imp_decode = $this->CI->hashids->decode($data);
		return implode(",",$imp_decode);
	}

    
	function check_session(){
		$sess_id = $this->CI->session->userdata('next_id');
		$sess_user = $this->CI->session->userdata('next_user');

		if(empty($sess_id) || empty($sess_user)){
			return redirect(base_url());
		}else{
			return TRUE;
		}

	}

    
	function result_callback($res){
		if($res){
			echo"success";
		}else{
			echo"error";
		}
	}

	function return_encode($data){
		return $this->encrypt_decrypt('encrypt', $data);
		//return $this->CI->encrypt->encode($data);
	}

	function return_decode($data){
		return $this->encrypt_decrypt('decrypt', $data);
		//return $this->CI->encrypt->decode($data);
	}

	function encrypt_decrypt($action, $string) {
	    $output = false;
	    $encrypt_method = "AES-256-CBC";
	    $secret_key = 'ORewByJmPYg5PFIC5EYMn3boghBV4p0t';
	    $secret_iv = 'gDO7GpJ4q6MV2ZTBcum4A5vXKfJh5lDo';
	    // hash
	    $key = hash('sha256', $secret_key);
	    
	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);
	    if ( $action == 'encrypt' ) {
	        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	        $output = base64_encode($output);
	    } else if( $action == 'decrypt' ) {
	        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }
	    return $output;
	}

    function session_make($email,$id){
		$sess_array = array(
				'next_user' => $this->return_encode($email),
				'next_id' => $this->return_encode($id)
			);
		$this->CI->session->set_userdata($sess_array);
	}


}//end of class