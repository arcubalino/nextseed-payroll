<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Companies extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('globalcall');
		$this->load->model('Company_model','CM');
        $this->globalcall->check_session();
	}

    function index(){
        $this->load->view('defaults/header');
        $this->load->view('defaults/sidebar');
        $this->load->view('defaults/navbar');
        $this->load->view('modules/companies');
        $this->load->view('defaults/footer');
        
    }

    function list(){
        $this->CM->list_comp();
    }


    function process(){
        $this->load->library('form_validation');
		$this->form_validation->set_rules('comp_name', 'Name', 'required');
		$this->form_validation->set_rules('comp_add', 'Address', 'required');
		$this->form_validation->set_rules('comp_desc', 'Description', 'required');
		if($this->form_validation->run() == FALSE){
			echo "error";
		}else{
			$res = $this->CM->process_company();
			$this->globalcall->result_callback($res);
		}
    }
    
    function remove(){
        $this->load->library('form_validation');
		$this->form_validation->set_rules('comp_id', 'ID', 'required');
		if($this->form_validation->run() == FALSE){
			echo "error";
		}else{
			$res = $this->CM->process_company();
			$this->globalcall->result_callback($res);
		}
    }

}//end of class