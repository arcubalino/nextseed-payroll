<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('globalcall');
		$this->load->model('Department_model','DM');
        $this->globalcall->check_session();
	}

    function index(){
        $this->load->view('defaults/header');
        $this->load->view('defaults/sidebar');
        $this->load->view('defaults/navbar');
        $this->load->view('modules/departments');
        $this->load->view('defaults/footer');
        
    }

    
    function list(){
        $this->DM->list_dept(0);
    }


    function process(){
        $this->load->library('form_validation');
		$this->form_validation->set_rules('dept_name', 'Name', 'required');
		$this->form_validation->set_rules('company_id', 'Company', 'required');
		if($this->form_validation->run() == FALSE){
			echo "error";
		}else{
			$res = $this->DM->process_dept();
			$this->globalcall->result_callback($res);
		}
    }
    
    function remove(){
        $this->load->library('form_validation');
		$this->form_validation->set_rules('dept_id', 'ID', 'required');
		if($this->form_validation->run() == FALSE){
			echo "error";
		}else{
			$res = $this->DM->process_dept();
			$this->globalcall->result_callback($res);
		}
    }


}//end of class