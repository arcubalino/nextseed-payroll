<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('globalcall');
		$this->load->model('App_model','AM');
	}

	public function index()
	{
		$check_session = $this->session->userdata('next_id');

		
		if(!empty($check_session)){
			$this->load->view('defaults/header');
			$this->load->view('defaults/sidebar');
			$this->load->view('defaults/navbar');
			$this->load->view('modules/dashboard');
			$this->load->view('defaults/footer');

		}else{
			$this->login();
		}
	}

	function login(){
		$this->load->view('defaults/header');
		$this->load->view('defaults/login');
		$this->load->view('defaults/footer');
	}

	function register(){

        $this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email Address:', 'required');
		$this->form_validation->set_rules('pass', 'Password:', 'required');
		$this->form_validation->set_rules('first_name', 'First Name:', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name:', 'required');
		if($this->form_validation->run() == FALSE){
			echo "error";
		}else{
			$res = $this->AM->process_register();
			$this->globalcall->result_callback($res);
		}
	}

	function validate(){
        $this->load->library('form_validation');
		$this->form_validation->set_rules('email_add', 'Email Address:', 'required');
		$this->form_validation->set_rules('password', 'Password:', 'required');
		if($this->form_validation->run() == FALSE){
			echo "error";
		}else{
			$res = $this->AM->process_validation();
			$this->globalcall->result_callback($res);
		}
	}

	function logout(){
        $this->session->unset_userdata(array('next_user','next_id'));
		//$this->session->sess_destroy();
		redirect(base_url());
	}

}//end of class
