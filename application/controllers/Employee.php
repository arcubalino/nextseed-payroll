<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('globalcall');
		$this->load->model('Employee_model','EM');
        $this->globalcall->check_session();
	}

    function index(){
        $this->load->view('defaults/header');
        $this->load->view('defaults/sidebar');
        $this->load->view('defaults/navbar');
        $this->load->view('modules/employees');
        $this->load->view('defaults/footer');
        
    }

    
    function list(){
        $this->EM->list_emp();
    }

    function dept_list($company_id){
        
		$this->load->model('Department_model','DM');
        $this->DM->list_dept($company_id);
    }
    


}//end of class