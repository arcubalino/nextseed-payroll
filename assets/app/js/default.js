
function loader(action){
    switch(action){
      case "on":
        $(".pre_blocker").show();
      break;
      case "off":
        $(".pre_blocker").hide();
      break;
    }
  }

  function toaster(message,alert){
    addStylePath('assets/toaster/toastr.css');
    // addScriptPath('assets/toaster/toaster.js');
    
          var i = -1;
            var toastCount = 0;
            var $toastlast;
    
                var shortCutFunction = alert;
                var msg = "<font size='3'>"+message+"</font>";
                var title = '';
                var $showDuration = "300";
                var $hideDuration = "500";
                var $timeOut = "500";
                var $extendedTimeOut = 1000;
                var $showEasing = "swing";
                var $hideEasing = "linear";
                var $showMethod = "fadeIn";
                var $hideMethod = "fadeOut";
                var toastIndex = toastCount++;
                var addClear = true;
    
                toastr.options = {
                    closeButton: true,
                    debug: true,
                    newestOnTop: false,
                    progressBar: false,
                    rtl: false,
                    positionClass: 'toast-top-right',
                    preventDuplicates: true,
                    onclick: null
                };
    
                $('#toastrOptions').text('Command: toastr["'
                        + shortCutFunction
                        + '"]("'
                        + title
                        + (title ? '", "' + title : '')
                        + '")\n\ntoastr.options = '
                        + JSON.stringify(toastr.options, null, 0)
                );
    
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;
    }

    
    function addStylePath(path){
        var link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = './'+path;
        link.media = 'all';
        document.getElementsByTagName('head')[0].appendChild(link);
    }

    
const purify = (dirty) => DOMPurify.sanitize(dirty);
    
function gmodal(action){
    switch(action){
      case "on":
        $("#myModal").modal({backdrop: 'static', keyboard: false});
      break;

      case "off":
        $(".modal-title,.modal-footer,.modal-body").html('');
        $("#close_modal").click();
        $(".modal-footer").html('');


      break;

      case "cancel":
           $(".modal-title,.modal-footer,.modal-body").html('');
        $("#close_modal").click();
      break;
    }
}

const field_trap = (target, action) => {
	switch (action) {
		case "clear":
			$(target).removeAttr("style");
			break;

		case "notice":
			$(target).css({
				outline: "none",
				"border-color": "red",
				"box-shadow": "0 0 10px red",
			});
			break;
	} //end of switch
};


const clear_data = (data) => {
	return data.replace(/\s/g, "");
};

function validate_email(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function csrf_token() {
	$token = $("meta[name='next_csrf']").attr("content");
	return $token;
}
