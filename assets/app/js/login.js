

const validate = () => {

    field_trap("#email_add,#password","clear");
    $email_add = $("#email_add").val();
    $password = $("#password").val();
    if(clear_data($email_add) == ""){
        $msg = "Please enter email address!";
        $target = "#email_add";
        $return = false;
    }else if(!validate_email($email_add)){
        $msg = "Please enter a valid email address!";
        $target = "#email_add";
        $return = false;
    }else if(clear_data($password) == ""){
        $msg = "Please enter password!";
        $target = "#password";
        $return = false;
    }else{
        $return = true;
    }
    
    if($return){
        var formData = new FormData();
        formData.append('next_csrf',csrf_token());
        formData.append('email_add',purify($email_add));
        formData.append('password',purify($password));
        fetch("./App/validate",{
            method: "POST",
            body: formData
        }).then($res => {
            return $res.text();
        }).then($out => {
            console.log($out)
            if(clear_data($out) == "success"){
                $(".new_field").val('')
                location.reload();
            }else{
                loader('off');
                toaster("Invalid email address or password!","error")
            }
        })
    }else{
        toaster($msg,"error")
        field_trap($target,'notice');
    }
}

const register = () => {
    field_trap("#new_email,#new_pass,#confirm_pass,#first_name,#last_name","clear");

    $new_email = $("#new_email").val();
    $new_pass = $("#new_pass").val();
    $confirm_pass = $("#confirm_pass").val();
    $first_name = $("#first_name").val();
    $last_name = $("#last_name").val();

    if(clear_data($new_email) == ""){
        $msg = "Please enter email address!";
        $target = "#new_email";
        $return = false;
    }else if(!validate_email($new_email)){
        $msg = "Please enter a valid email address!";
        $target = "#new_email";
        $return = false;
    }else if(clear_data($new_pass) == ""){
        $msg = "Please enter password!";
        $target = "#new_pass";
        $return = false;
    }else if(clear_data($confirm_pass) == ""){
        $msg = "Please confirm password!";
        $target = "#confirm_pass";
        $return = false;
    }else if(clear_data($confirm_pass) != clear_data($new_pass)){
        $msg = "Password does not match!";
        $target = "#new_pass,#confirm_pass";
        $return = false;
    }else if(clear_data($first_name) == ""){
        $msg = "Please enter first name!";
        $target = "#first_name";
        $return = false;
    }else if(clear_data($last_name) == ""){
        $msg = "Please enter last name!";
        $target = "#last_name";
        $return = false;
    }else{
        $return = true;
    }

    if($return){
        loader('on');
        var formData = new FormData();
        formData.append('next_csrf',csrf_token());
        formData.append('email',purify($new_email));
        formData.append('pass',purify($new_pass));
        formData.append('first_name',purify($first_name));
        formData.append('last_name',purify($last_name));
        
        fetch("./App/register",{
            method: "POST",
            body: formData
        }).then($res => {
            return $res.text();
        }).then($out => {
            console.log($out)
            if(clear_data($out) == "success"){
                $(".new_field").val('')
                toaster("Successfully created an account!","success");
                cancel_account();
            }else if(clear_data($out) == "exist"){
                toaster("Email address is already registered!","error")
            }else{
                toaster("Error has been encountered!","error")
            }
            loader('off');
        })

    }else{
        field_trap($target,"notice")
        toaster($msg,"error")
    }

}

const create_account = () => {
    $("#titlex").html('Fill out account details');

    $("#sign_in_div").hide();
    $("#create_div").show();

}

const cancel_account = () => {
    $("#titlex").html('Sign in with credentials');
    $("#sign_in_div").show();
    $("#create_div").hide();
}



