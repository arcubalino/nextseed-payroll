$(function(){
    //employees();
})


const create = (comp_id) => {
    $(".modal-title").text('');
    $(".modal-body").html(`
        <input type="hidden" id="emp_id">
        <div class="form-group">
            <label>COMPANY:</label>
            <select class="form-control" onchange="departments(this.value)" id="comp_list">
                <option value=''>Select Company:</option>
            </select>
        </div>
        <div class="form-group">
            <label>DEPARTMENT:</label>
            <select class="form-control" disabled="disabled" id="dept_list">
                <option value=''>Select Department:</option>
            </select>
        </div>
        <div class="form-group">
            <label>FIRST NAME:</label>
            <input type="text" class="form-control" id="first_name">
        </div>
        <div class="form-group">
            <label>LAST NAME:</label>
            <input type="text" class="form-control" id="last_name">
        </div>
    `);
    $(".modal-footer").html(`<button class="btn btn-success" onclick="save()"><i class="fa fa-paper-plane"></i> SAVE</button>`);
    companies(comp_id);
    gmodal('on')
}

const companies = (comp_id) => {

    loader('on');
    fetch("./Companies/list")
    .then($res => {
        return $res.json();
    }).then($out => {
        if($out.length == 0){
            toaster("No record found!","error");
        }else{
            $(".appended_comp").remove();
            for(var i=0; i < $out.length; i++){
                $comp_id = $out[i]['comp_id'];

                if($comp_id == comp_id){
                    $selected = 'selected="selected"';
                }else{
                    $selected = '';
                }

                $("#comp_list").append(`<option class="appended_comp" ${$selected} value="${$comp_id}">${$out[i]['name']}</option>`);
            }
        }
        loader('off')
    })
}

const departments = (comp_id) => {
   
    if(comp_id == ""){
        toaster("Please select company!","error")
    }else{
        
        $(".appended_dept").remove();
        loader('on');
        fetch("./Employee/dept_list/"+comp_id)
        .then($res => {
            return $res.json();
        }).then($out => {
            console.log($out)
            if($out.length == 0){
                toaster("No record found!","error");
            }else{
                for(var i=0; i < $out.length; i++){
                    $dept_id = $out[i]['dept_id'];
                    $company_id = $out[i]['company_id'];

                    $("#dept_list").append(`<option class="appended_dept" ${$selected} value="${$dept_id}">${$out[i]['dept_name']}</option>`);
                }
                $("#dept_list").attr('disabled',false);
            }
            loader('off')
        })
    }
}



