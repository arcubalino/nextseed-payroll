$(function(){
    departments();
})


const create = (comp_id) => {
    $(".modal-title").text('');
    $(".modal-body").html(`
        <input type="hidden" id="dept_id">
        <div class="form-group">
            <label>NAME:</label>
            <input type="text" class="form-control" id="dept_name">
        </div>
        <div class="form-group">
            <label>COMPANY:</label>
            <select class="form-control" id="comp_list">
                <option value=''>Select Company:</option>
            </select>
        </div>
    `);
    $(".modal-footer").html(`<button class="btn btn-success" onclick="save()"><i class="fa fa-paper-plane"></i> SAVE</button>`);
    companies(comp_id);
    gmodal('on')
}

const edit = (dept_id,comp_id) => {
    create(comp_id);
    $dept_name = $("#dept_name_of_"+dept_id).html();
    $("#dept_name").val($dept_name)
    $("#dept_id").val(dept_id);
}

const save = () => {
    field_trap("#dept_name,#comp_list","clear");
    $dept_id = $("#dept_id").val();
    $dept_name = $("#dept_name").val();
    $comp_list = $("#comp_list").val();
    $dup = false;

    $(".department_of_"+$company_id).each(function(){
        $valx = $(this).html();
        if(clear_data($valx) == clear_data($dept_name) && $dept_id == ""){
            $dup = true;
            return false;
        }
    })

    if($dept_name == ""){
        $msg = "Please enter department name!";
        $target = "#dept_name";
        $return = false;
    }else if($dup){
        $msg = "Department already exist!";
        $target = "#dept_name";
        $return = false;
    }
    else if($comp_list == ""){
        $msg = "Please select company!";
        $target = "#comp_list";
        $return = false;
    }else{  
        $return = true;
    }

    if($return){
        var formData = new FormData();
        formData.append('next_csrf',csrf_token());
        formData.append('dept_id',purify($dept_id));
        formData.append('dept_name',purify($dept_name));
        formData.append('company_id',purify($comp_list));
        formData.append('action','save');

        fetch("./Department/process",{
            method: "POST",
            body: formData
        }).then($res => {
            return $res.text();
        }).then($out => {
            console.log($out);
            if(clear_data($out) == "success"){
                departments();
                gmodal('off')
                toaster("Successfully saved department!","success");
            }else{
                toaster("Error has been encountered!","error")
            }
            loader('off')
        })
    }else{
        toaster($msg,"error");
        field_trap($target,"notice");
    }
}

const companies = (comp_id) => {

    loader('on');
    fetch("./Companies/list")
    .then($res => {
        return $res.json();
    }).then($out => {
        if($out.length == 0){
            toaster("No record found!","error");
        }else{
            $(".appended_comp").remove();
            for(var i=0; i < $out.length; i++){

                $comp_id = $out[i]['comp_id'];
                if(comp_id == $comp_id){
                    $selected = 'selected="selected"';
                }else{
                    $selected = '';
                }
                $("#comp_list").append(`<option ${$selected} class='appended_comp' value="${$comp_id}">${$out[i]['name']}</option>`)
            }
        }
        loader('off')
    })
}


const remove = (dept_id) => {
    if(confirm("Are you sure to remove this department?")){
        loader('on');
        var formData = new FormData();
        formData.append('next_csrf',csrf_token());
        formData.append('action','remove');
        formData.append('dept_id',dept_id);

        fetch("./Department/remove",{
            method: "POST",
            body: formData
        }).then($res => {
            return $res.text();
        }).then($out => {
            console.log($out);
            if(clear_data($out) == "success"){
                $("#dept_of_"+dept_id).remove();
                toaster("Successfully removed company!","success");
            }else{
                toaster("Error has been encountered!","error")
            }
            loader('off')
        })
    }
}


const departments = () => {

    loader('on');
    fetch("./Department/list")
    .then($res => {
        return $res.json();
    }).then($out => {
        if($out.length == 0){
            toaster("No record found!","error");
        }else{
            $(".appended_dept").remove();
            for(var i=0; i < $out.length; i++){
                $dept_id = $out[i]['dept_id'];
                $company_id = $out[i]['company_id'];

                $("#comp_here").append(`
                    <tr class="appended_dept" id="dept_of_${$dept_id}">
                        <td id="dept_comp_of_${$dept_id}">${$out[i]['company_name']}</td>
                        <td class="department_of_${$company_id}" id="dept_name_of_${$dept_id}">${$out[i]['dept_name']}</td>
                        <td>${$out[i]['user_name']}</td>
                        <td>${$out[i]['modified_at']}</td>
                        <td>
                            <a href="javascript:void(0)" style="color:green;" onclick="edit('${$dept_id}','${$company_id}')">
                                <i class="fa fa-edit"></i> edit
                            </a>
                            &nbsp;
                            <a href="javascript:void(0)" style="color:red;" onclick="remove('${$dept_id}')">
                                <i class="fa fa-times"></i> remove
                            </a>
                        </td>
                    </tr>
                `);
            }
        }
        loader('off')
    })
}
