$(function(){
    companies();
})

const create_comp = () => {
    $(".modal-title").text('');
    $(".modal-body").html(`
        <input type="hidden" id="comp_id">
        <div class="form-group">
            <label>NAME:</label>
            <input type="text" class="form-control" id="comp_name">
        </div>
        <div class="form-group">
            <label>ADDRESS:</label>
            <input type="text" class="form-control" id="comp_add">
        </div>
        <div class="form-group">
            <label>DESCRIPTION:</label>
            <textarea class="form-control" id="comp_desc"></textarea>
        </div>
    `);
    $(".modal-footer").html(`<button class="btn btn-success" onclick="save_company()"><i class="fa fa-paper-plane"></i> SAVE</button>`);
    gmodal('on')
}

const edit = (comp_id) => {
    create_comp();

    $comp_name = $("#comp_name_of_"+comp_id).html();
    $comp_add = $("#comp_add_of_"+comp_id).html();
    $comp_desc = $("#comp_desc_of_"+comp_id).html();
    
    $("#comp_id").val(comp_id);
    $("#comp_name").val($comp_name);
    $("#comp_add").val($comp_add);
    $("#comp_desc").val($comp_desc);
}


const save_company = () => {
    field_trap("#comp_name,#comp_add,#comp_desc","clear")
    $comp_name = $("#comp_name").val();
    $comp_add = $("#comp_add").val();
    $comp_desc = $("#comp_desc").val();
    $comp_id = $("#comp_id").val();

    $dup = false;
    $(".all_comp").each(function(){
        $valx = $(this).html();
    
        if(clear_data($valx) == clear_data($comp_name) && $comp_id == ""){
            $dup = true;
            return false;
        }
    })

    if($comp_name == ""){
        $msg = "Please enter company name!";
        $target = "#comp_name";
        $return = false;
    }else if($dup){
        $msg = "Company already exist!";
        $target = "#comp_name";
        $return = false;
    }else if($comp_add == ""){
        $msg = "Please enter company address!";
        $target = "#comp_add";
        $return = false;
    }else if($comp_desc == ""){

        $msg = "Please enter company description!";
        $target = "#comp_desc";
        $return = false;
    }else{
        $return = true;
    }

    if($return){
        loader('on');
        var formData = new FormData();
        formData.append('next_csrf',csrf_token());
        formData.append('comp_name',purify($comp_name));
        formData.append('comp_add',purify($comp_add));
        formData.append('comp_desc',purify($comp_desc));
        formData.append('comp_id',purify($comp_id));
        formData.append('action','save');

        fetch("./Companies/process",{
            method: "POST",
            body: formData
        }).then($res => {
            return $res.text();
        }).then($out => {
            console.log($out);
            if(clear_data($out) == "success"){
                companies();
                gmodal('off')
                toaster("Successfully saved company!","success");
            }else{
                toaster("Error has been encountered!","error")
            }
            loader('off')
        })


    }else{
        toaster($msg,"error")
        field_trap($target,"notice");
    }
}

const companies = () => {

    loader('on');
    fetch("./Companies/list")
    .then($res => {
        return $res.json();
    }).then($out => {
        if($out.length == 0){
            toaster("No record found!","error");
        }else{
            $(".appended_comp").remove();
            for(var i=0; i < $out.length; i++){
                $comp_id = $out[i]['comp_id'];

                $("#comp_here").append(`
                    <tr class="appended_comp" id="comp_of_${$comp_id}">
                        <td class="all_comp" id="comp_name_of_${$comp_id}">${$out[i]['name']}</td>
                        <td id="comp_add_of_${$comp_id}">${$out[i]['address']}</td>
                        <td id="comp_desc_of_${$comp_id}">${$out[i]['description']}</td>
                        <td>${$out[i]['user_name']}</td>
                        <td>${$out[i]['modified_at']}</td>
                        <td>
                            <a href="javascript:void(0)" style="color:green;" onclick="edit('${$comp_id}')">
                                <i class="fa fa-edit"></i> edit
                            </a>
                            &nbsp;
                            <a href="javascript:void(0)" style="color:red;" onclick="remove_comp('${$comp_id}')">
                                <i class="fa fa-times"></i> remove
                            </a>
                        </td>
                    </tr>
                `);
            }
        }
        loader('off')
    })
}

const remove_comp = (comp_id) => {
    if(confirm("Are you sure to remove this company?")){
        loader('on');
        var formData = new FormData();
        formData.append('next_csrf',csrf_token());
        formData.append('action','remove');
        formData.append('comp_id',comp_id);

        fetch("./Companies/remove",{
            method: "POST",
            body: formData
        }).then($res => {
            return $res.text();
        }).then($out => {
            console.log($out);
            if(clear_data($out) == "success"){
                $("#comp_of_"+comp_id).remove();
                toaster("Successfully removed company!","success");
            }else{
                toaster("Error has been encountered!","error")
            }
            loader('off')
        })
    }
}