/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.4.17-MariaDB : Database - nextseed
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nextseed` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `nextseed`;

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` int(255) DEFAULT 0 COMMENT '0 - Active | 1 - Inactive',
  `modified_by` int(255) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `companies` */

insert  into `companies`(`id`,`name`,`address`,`description`,`status`,`modified_by`,`modified_at`) values (1,'Next Seed','Test Add','Test Descxxx',0,1,'2021-05-11 21:02:47'),(2,'aa','bb','ccc',1,1,'2021-05-11 20:59:13'),(3,'Argon','Test','Test',0,1,'2021-05-11 21:13:23');

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `company_id` int(255) DEFAULT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `status` int(255) DEFAULT 0 COMMENT '0 - Active | 1 - Inactive',
  `modified_by` int(255) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `departments` */

insert  into `departments`(`id`,`company_id`,`name`,`status`,`modified_by`,`modified_at`) values (1,1,'Engineering',0,1,'2021-05-11 21:29:55'),(2,3,'ITx',1,1,'2021-05-11 21:57:39'),(4,1,'Planning',0,1,'2021-05-11 21:49:03'),(5,3,'aaa',1,1,'2021-05-11 21:57:52');

/*Table structure for table `employees` */

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(255) DEFAULT NULL,
  `first_name` text DEFAULT NULL,
  `middle_name` text DEFAULT NULL,
  `last_name` text DEFAULT NULL,
  `emp_status` int(255) DEFAULT 0 COMMENT '0 - Probi | 1 - Regular | 2 - Resigned',
  `status` int(255) DEFAULT 0 COMMENT '0 - Active | 1 - Inactive',
  `modified_by` int(255) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `employees` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `first_name` text DEFAULT NULL,
  `last_name` text DEFAULT NULL,
  `status` int(255) DEFAULT 0 COMMENT '0 - Active | 1 - Inactive',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`first_name`,`last_name`,`status`,`created_at`) values (1,'ran.revolution@gmail.com','2f8a4431b0ba44c819be3ff2afcb9e06','Ranil','Jaramillo',0,'2021-05-11 19:55:18');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
